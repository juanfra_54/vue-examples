new Vue({
    el: '#app',
    data: {
        isActive: true,
        hasError: false,
        activeClass: 'active',
        errorClass: 'text-danger',
        activeColor: 'red',
        fontSize: 30,
        baseStyles: {
            color: 'red',
            fontSize: '15px'
        },
        overridingStyles: {
            color: 'blue',
            fontSize: '20px'
        }
    }
  })