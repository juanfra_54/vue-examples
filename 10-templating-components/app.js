Vue.component('string-template-component', {
    template: '<div><p>Hello, I am a globalstring templated component</p></div>'
});

Vue.component('external-node-component', {
  template: '#external-node-template'
});

Vue.component('inline-template-component', {
  data(){
    return {
      prova:'hello'
    }
  }
});
  
new Vue({
    el: '#app',
    data() {
      return {
        greeting: 'Hello people!', 
        question: 'Whats up?',
      };
    },
  });