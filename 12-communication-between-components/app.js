var eventBus = new Vue();

Vue.component('app-component1', {
    data: function() {
        return {
            name: 'David Hayden'
        }
    },
    methods: {
        changeName: function() {
            eventBus.$emit('nameChange', this.name);
            this.name = '';
            this.$refs.textbox.focus();
        }
    },
    template: '<div class="well"><h3>Component 1</h3><hr/><form><div class="form-group"><label for="name">Name</label><input type="text" v-model="name" id="name" class="form-control" placeholder="name" @keydown.enter.prevent="changeName" ref="textbox"></div><div class="form-group"><button class="btn btn-primary" @click.prevent="changeName">Change Name</button></div></form></div>'
})

Vue.component('app-component2', {
    data: function() {
        return {
            name: 'David Hayden'
        }
    },
    template: '<div class="well"><h3>Component 2</h3><hr/><p>Name: {{ name }}</p></div>',
    created: function() {
        var that=this;
        eventBus.$on('nameChange', function(newName) {
            that.name = newName;
        })
    }
})

new Vue({
    el: '#app'
})